require([
    "esri/Map",
    "esri/views/MapView",
    "esri/request",
    "esri/layers/FeatureLayer",
    "esri/widgets/Legend",
    "esri/widgets/TimeSlider",
    "esri/widgets/Popup",
    "esri/widgets/Expand",
    "esri/widgets/LayerList",
    "esri/core/watchUtils",
    "esri/PopupTemplate",
    "esri/tasks/support/Query",
    "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js",
    "dojo/domReady!"
], function(Map, MapView, esriRequest, FeatureLayer, Legend,TimeSlider, Popup, Expand, LayerList, watchUtils, PopupTemplate, Query, Chart) {

    let map = new Map({
        basemap: "dark-gray"
    });

    let view = new MapView({
        container: "viewDiv",
        map: map,
        center: [6.803, 53.464],
        zoom: 10
    });

    const layerList = new LayerList({
        view: view,
    });
    view.ui.add(layerList, "top-right");

    // add datapoints
    let options = {
        query: {
            limit: 30
        },
        responseType: "json"
    };

    let features = [];
    let objectID = 1;

    esriRequest("/mapdata", options).then(function (response) {
        let farms = JSON.stringify(response, null, 2);
        response.data.forEach(farm => {

            // Extract data
            let geometry = {
                type: "point",
                x: parseFloat(farm['latLon'][1]),
                y: parseFloat(farm['latLon'][0])
            };

            let attr = {
                ObjectID: objectID,
                FarmID: farm.FarmID,
                date: farm.date,
                state: farm.state,
                latLon: farm.latLon
            };

            features.push({
                geometry: geometry,
                attributes: attr

            });
            objectID++;
        });

        const fields = [
            {
                name: "ObjectID",
                alias: "ObjectID",
                type: "oid"
            }, {
                name: "state",
                alias: "state",
                type: "string"
            }, {
                name: "FarmID",
                alias: "FarmID",
                type: "string"
            }, {
                name: "date",
                alias: "date",
                type: "date"
            }, {
                name: "latLon",
                alias: "latLon",
                type: "string"
            }];

        const popuptemplate = new PopupTemplate({
            title: "FarmID : {FarmID}",
            content: [
                {
                    type: "fields",
                    fieldInfos: [
                        {
                            fieldName: "date",
                            label: "Date",
                            visible: true
                        },
                        {
                            fieldName: "state",
                            label: "State",
                            visible: true
                        },
                        {
                            fieldName: "latLon",
                            label: "Coordinates",
                            visible: true
                        }
                    ]
                },
                {
                    // You can set a media element within the popup as well. This
                    // can be either an image or a chart. You specify this within
                    // the mediaInfos. The following creates a pie chart in addition
                    // to two separate images. The chart is also set up to work with
                    // related tables. Similar to text elements, media can only be set within the content.
                    type: "media", // MediaContentElement
                    mediaInfos: [
                        {
                            title: "<b>Count by infection</b>",
                            type: "line-chart",
                            caption: "",
                            value: {
                                fields: "state",
                                visible: true,
                                normalizeFieldField: {

                                }
                            }
                        }
                        ]}
            ]
        });

        const allRenderer = {
            type: "simple", // autocasts as new SimpleRenderer()
            objectIdField: "ObjectID",
            label: "Investigated farm",
            field:"FarmID",
            // opacity: 0.5,
            symbol: {
                type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                size: 9,
                color: [255, 255, 255, 0.05],
                outline: null
            }
        };

        const allLayer = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            renderer: allRenderer,
            title: "Every investigated farm",
            popupTemplate: popuptemplate,
            fields: fields,
            visible: false
        });

        // Symbol for state of infection
        const contaminated = {
            type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
            url: 'https://static.arcgis.com/images/Symbols/Firefly/FireflyD20.png',
            width: '30px',
            height: '30px'
        };

        const clean = {
            type: "simple-marker",  // autocasts as new PictureMarkerSymbol()
            size: 7,
            color: [255, 255, 255, 0.05],
            outline: null
        };

        const contaminationRenderer = {
            type: "unique-value", // autocasts as new UniqueValueRenderer()
            legendOptions: {
                title: "State of contamination"
            },
            field: "state",
            uniqueValueInfos: [
                {
                    value: "TRUE", // code for infected
                    symbol: contaminated,
                    label: "Contaminated with Salmonella"
                },
                {
                    value: "FALSE", // code for non-infected
                    symbol: clean,
                    label: "Clean tank"
                }
            ]
        };

        const layer = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            renderer: contaminationRenderer,
            popupTemplate: popuptemplate,
            fields: fields,
            title: "Current state of infection",
            definitionExpression: "date",
            timeInfo: {
                startField: "date", // name of the date field
                interval: { // specify time interval for
                    unit: "months",
                    value: 3
                }
            }
        });

        // Add FeatureLayer to the map.
        map.add(allLayer);
        map.add(layer);

        <!-- LEGEND -->
        const legend = new Legend({
            view: view,
            container: document.createElement("infoDiv"),
            layerInfos: [
                {
                    layer: layer,
                    title: "Infection of Salmonella at Farms (Groningen)"
                },
                {
                    layer: allLayer,
                    title: "Infection of Salmonella at Farms (Groningen)"
                }
            ]
        });
        // Make the legend expandable
        let bgExpand = new Expand({
            view: view,
            content: legend,
            expandTooltip: "Legend",
            expanded: true
        });

        view.ui.add(bgExpand, "top-right");

        <!-- TIMESLIDER -->
        const timeSlider = new TimeSlider({
            container: "timeSliderDiv",
            playRate: 1000,
            stops: {
                interval: {
                    value: 4,
                    unit: "months"
                }
            }
        });
        view.ui.add(timeSlider, "bottom");

        let layerView;

        view.whenLayerView(layer).then(function(lv) {
            layerView = lv;
            const fullTimeExtent = layer.timeInfo.fullTimeExtent;
            // start time of the time slider - 1/4/2017
            const start = new Date(2017, 4, 1);
            // set time slider's full extent to
            // from start until end date of layer's fullTimeExtent
            timeSlider.fullTimeExtent = {
                start: start,
                end: layer.timeInfo.fullTimeExtent.end
            };

            const end = new Date(start);
            // end of current time extent for time slider
            // showing farms with 4 months interval
            end.setDate(end.getDate() + 120);

            // Values property is set so that timeslider
            // widget show the first day. We are setting
            // the thumbs positions.
            timeSlider.values = [start, end];
        });

        // watch for time slider timeExtent change
        timeSlider.watch("timeExtent", function() {
            // only show farms up until the end of
            // timeSlider's current time extent.
            layer.definitionExpression =
                "date <= " + timeSlider.timeExtent.end.getTime();

            // now gray out earthquakes that happened before the time slider's current
            // timeExtent... leaving footprint of earthquakes that already happened
            layerView.effect = {
                filter: {
                    timeExtent: timeSlider.timeExtent,
                    geometry: view.extent
                },
                excludedEffect: "grayscale(5%) opacity(10%)"
            };
        });
    });
});
