SET GLOBAL time_zone = '+5:30';

create DATABASE if not exists FarmDB;
USE FarmDB;
sHOW TABLES ;
DROP TABLE IF EXISTS Farms_copy;
DROP TABLE IF EXISTS Farms;

CREATE TABLE IF NOT EXISTS Farms (
                                     FarmID VARCHAR(5),
                                     Latitude VARCHAR(20),
                                     Longitude VARCHAR(20),
                                     IDate VARCHAR(50),
                                     Contamination CHAR(1)
);


CREATE TABLE Farms_copy LIKE Farms;
LOAD DATA LOCAL INFILE '/homes/mlbloembergen/thema10/practicum/salmonellosis/salmonellosis/data/th10-export.csv'
    INTO TABLE Farms
    FIELDS TERMINATED BY ';'
    OPTIONALLY ENCLOSED BY ""
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;

-- Remove Duplicates
INSERT INTO Farms_copy
SELECT * FROM Farms
GROUP BY FarmID, IDate; -- column that has duplicate values

DROP TABLE Farms;
ALTER TABLE Farms_copy RENAME TO Farms
;
#


# SELECT * FROM Farms_copy;
# SELECT * FROM Farms where FarmID =null;
DROP TABLE IF EXISTS FarmInfo;
CREATE TABLE FarmInfo
AS (SELECT DISTINCT (Farms.FarmID), Farms.Latitude, Farms.Longitude FROM Farms);

DROP TABLE IF EXISTS Measurements;
CREATE TABLE Measurements
AS (SELECT Farms.FarmID, Farms.IDate, Farms.Contamination FROM Farms);

SELECT * FROM FarmInfo;
SELECT * FROM Measurements;
SELECT * FROM Farms;



# ALTER TABLE FarmInfo
# ADD num_measurements INT;

# INSERT INTO FarmInfo (FarmDB.FarmInfo.num_measurements)
# SELECT COUNT(FarmDB.Measurements.FarmID) FROM FarmDB.Measurements GROUP BY FarmDB.Measurements.FarmID;