package servlets;
/*
 * @author Jildou Haaijer
 * @version 0.01
 */
import config.WebConfig;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

/**
 * This servlet is created for the View data page, where it is possible to filter the data.
 * It contains of the methods: doPost, doGet and process.
 */
@WebServlet(name = "TableServlet", urlPatterns = "/Datatable", loadOnStartup = 1)
public class TableServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    // doPost method
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }

    // doGet method
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }

    // process method
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("Datatable", ctx, response.getWriter());
    }
}
