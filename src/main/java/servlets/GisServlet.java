package servlets;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import config.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
/**
 * This servlet is created for the GIS page. The methods are doGet and doPost.
 */
@WebServlet(name = "GisServlet", urlPatterns = "/Gis")
public class GisServlet extends HttpServlet {
    private TemplateEngine templateEngine;


    @Override
    public void init() throws ServletException {
        final ServletContext servletcontext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletcontext);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        templateEngine.process("Gis", ctx, response.getWriter());

    }
}

