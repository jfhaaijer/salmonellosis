package servlets;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import config.WebConfig;
import org.thymeleaf.context.WebContext;
import service.DatabaseDataInserter;
import service.FileHandler;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Locale;

/**
 * This Servlet requests the data inserted by the user at the Add new data page and adds it to a database table. Either
 * a single data point or a file can be added.
 */
@WebServlet(name = "FileUploadServlet", urlPatterns = {"/FileUploadServlet"})
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        ServletConfig c = this.getServletConfig();

        Part filePart = request.getPart("fileToUpload");

        // Get the file chosen by the user
        String uploadpath = (c.getInitParameter("data_dir") + "/uploaded-files/" +
                filePart.getSubmittedFileName());

        // Check if file is a csv file
        if (filePart.getSubmittedFileName().endsWith(".csv")) {

            // Copy file to "data/uploaded-files" folder
            InputStream fileInputStream = filePart.getInputStream();
            File fileToSave = new File(c.getInitParameter("data_dir") + "/uploaded-files/" +
                    filePart.getSubmittedFileName());
            Files.copy(fileInputStream, fileToSave.toPath(), StandardCopyOption.REPLACE_EXISTING);

//            String fileUrl = "http://localhost:8080/Add/" + filePart.getSubmittedFileName();

            // get the number to adjust the coordinates with
            double code = Double.parseDouble(request.getParameter("code"));

            Locale locale = request.getLocale();
            WebContext ctx = new WebContext(
                    request,
                    response,
                    request.getServletContext(),
                    locale);

            try {
                FileHandler filehandler = new FileHandler();
                System.out.println("CSVFILE inserted.....");
                // Check if the file is legit
                if (filehandler.isCorrectFile(uploadpath)) {
                    if (code != 0) { // if the number is 0, the coordinates do not need to change
                        // Reverse coordinates with FileHandler method
                        String newpath = filehandler.reverseCoordinates(filePart.getSubmittedFileName(),
                                uploadpath, code);
                        // Insert new file ("data/changed-files/.." into the database)
                        DatabaseDataInserter.InsertFile(newpath);
                    } else {
                        // Insert new file ("data/uploaded-files/.." into the database)
                        DatabaseDataInserter.InsertFile(uploadpath);
                    }

                    // Return a statement to the user
                    ctx.setVariable("fileApproved", "File has been added");
                } else ctx.setVariable("fileApproved", "Incorrect file, please upload a correct one.");
            } catch (IOException ignored) {
            }

            final ServletContext servletContext = super.getServletContext();
            WebConfig.createTemplateEngine(servletContext).process("Add", ctx, response.getWriter());
        } else {
            // If the given file is not a .csv file return a statement about only uploading csv
            Locale locale = request.getLocale();
            WebContext ctx = new WebContext(
                    request,
                    response,
                    request.getServletContext(),
                    locale);

            // Return a statement to the user
            ctx.setVariable("fileApproved", "Please only upload CSV files");

            final ServletContext servletContext = super.getServletContext();
            WebConfig.createTemplateEngine(servletContext).process("Add", ctx, response.getWriter());
        }
    }
}
