package service;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import com.google.gson.Gson;
import model.Farm;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains methods for the retrieval of the data from the database.
 * It contains the following methods: getFarms, RetrieveData, SQLToJsonWriter.
 * GetFarms is a method that can be used to call the SQLtoJsonWriter and returns a json file. RetrieveData makes a
 * connection to the database. SQLtoJsonWriter writes the data to a json file.
 */
public class FarmDataFetcher {

    /**
     * Fetches the data from the database table.
     * @return JSON Formatted list of farms
     */
    public String getFarms() throws Exception {
        // write data to json file
        return SQLToJsonWriter();
    }


    /**
     * Creates a connection to the database and retrieves everything from the farms table.
     * @return A resultSet of the farms table data
     * @throws Exception throws exception when the connection fails
     */
    private static ResultSet RetrieveData() throws Exception {
        Connection conn;
        Statement stmt;
        //STEP 2: Register JDBC driver
        Class.forName("com.mysql.cj.jdbc.Driver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        conn = SQLconnector.initializeDatabase();
        System.out.println("Connected database successfully...");

        //STEP 4: Execute a query
        System.out.println("Retrieving records from the table...");
        stmt = conn.createStatement();

        String query = "Select * from salmonellosis.Farms";
        ResultSet rs = stmt.executeQuery(query);
        System.out.println("Retrieved records from the table...");
        return rs;
    }

    /**
     * Given a String containing the full Farms response, converts this to a list of Farm objects and
     * subsequently converting and returning this as a JSON String.
     * @return a json file with every farm measurement as an object
     */
    private String SQLToJsonWriter() throws Exception {
        List<Farm> data = new ArrayList<>();

        //Creating a JSONObject object
        JSONObject jsonObject = new JSONObject();
        //Creating a json array
        JSONArray array = new JSONArray();
        ResultSet rs = RetrieveData();
        //Inserting ResutlSet data into the json object
        while(rs.next()) {
            Farm eq = new Farm(
                        rs.getInt("FarmID"),  // ID
                        rs.getString("Latitude"),  // lat
                        rs.getString("Longitude"),  // Long
                        rs.getString("IDate"),  // date
                        rs.getString("Contamination")); // Contamination

            array.add(eq);
            data.add(eq);

        }
        // Add the data array as a json object
        jsonObject.put("Farm_data", data);

        // Write data to file
        try {
            FileWriter file = new FileWriter("../../../data/json_array_output.json");
            file.write(jsonObject.toJSONString());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("JSON file created......");
        return new Gson().toJson(data);
    }
}
