package service;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import com.opencsv.CSVReader;
import java.io.*;
import java.util.Arrays;

/**
 *  De FileHandler class contains methods for checking and changeing the contents of a file. It contains the
 *  following methods: isCorrectFile en reverseCoordinates. The isCorrectFile method checks if a csvfile has te right
 *  data (FarmID, Latitude, Longitude, Date and State of contamination).
 */
public class FileHandler {

    /**
     * This method checks if a file has the correct header (FarmID, Latitude, Longitude, Date and State of
     * contamination).
     * @param file a csv file with the farm data.
     * @return returns False is file is incorrect and true if it is correct.
     * @throws IOException if the file cannot be opened.
     */
    public Boolean isCorrectFile(String file) throws IOException {
        CSVReader csvReader;
        Boolean isCorrectFile = Boolean.FALSE;
        StringBuilder line = new StringBuilder();
        // Read file
        csvReader = new CSVReader(new FileReader(file));
        // Read the first line from the data
        for (String i:csvReader.readNext()) {
            line.append(i);
        }
        // Check if header matches FarmID;Latitude;Longitude;Date;Contamination
        if (line.toString().matches("FarmID;Latitude;Longitude;Date;Contamination")){
            System.out.println(Arrays.toString(csvReader.readNext()));
            isCorrectFile = Boolean.TRUE;
        }
        return isCorrectFile;
    }

    /**
     * Change coordinates (latitude and longitude) with the given numberand write this to a new file.
     * @param filename csv file with farm data
     * @param filepath path to the new file ("data/changed-files")
     * @param code the number to change the coordinates with
     * @return a new file with adjusted coordinates.
     */
    public String reverseCoordinates(String filename, String filepath, double code) {
        String newfile = "../../../data/changed-files/";
        String line;

        try (
                BufferedReader br = new BufferedReader(new FileReader(filepath))) {
                FileWriter fileWriter = new FileWriter(newfile+filename, true);
                // Write header
                fileWriter.write("FarmID;Latitude;Longitude;Date;Contamination");

                // Adjust for each line the latitude and longitude with the given number
                while ((line = br.readLine()) != null) {
                    if (line.startsWith("FarmID")) { // Skip header
                        continue;
                    }
                    // Use comma as separator
                    String[] fields = line.split(";");

                    // Change coordinates
                    String latitude = String.valueOf(Double.parseDouble(fields[1]) - code);
                    String longitude = String.valueOf(Double.parseDouble(fields[2]) - code);

                    // Write new line with adjusted coordinates
                    String newline = "\n"
                            + fields[0] + ";"
                            + latitude + ";"
                            + longitude + ";"
                            + fields[3] + ";"
                            + fields[4] + ";";

                    fileWriter.write(newline); // Write new line to file

                }
                fileWriter.close(); // Close file writer

        } catch (IOException e) {
            e.printStackTrace();
        }
        // Return the new file
        return newfile + filename;
    }
}