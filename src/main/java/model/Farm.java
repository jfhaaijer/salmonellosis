package model;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * This class creates a Farm object. It contains the following data: Farm ID, longitude and latitude, the date the
 * measurement was taken and the state of contamination of that farm at that date.
 */
public class Farm {
    private long date;
    private String latitude;
    private String longitude;
    private String state;
    private String FarmID;


  /**
   * @param FarmID the Farm ID int
   * @param latitude latitude String
   * @param longitude longitude String
   * @param date date of the measurement String
   * @param contamination state of contamination either TRUE or FALSE as String
   */
    public Farm(int FarmID, String latitude, String longitude, String date, String contamination) {
        setFarmID(FarmID);
        setCoordinates(latitude, longitude);
        setDate(date);
        setState(contamination);
    }

    public String getFarmID(){
        return FarmID;
    }

    public void setFarmID(int ID){
        this.FarmID = String.valueOf(ID);
    }

    public long getDate() {
        return date;
    }

    public void setDate(String date) {
        long dateInLong = 0;
        // Change data format
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
          // Change date to long
            Date Epochdate = formatter.parse(date);
            dateInLong = Epochdate.getTime();
        }catch (ParseException e) {
            e.printStackTrace();
        } this.date = dateInLong;

    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setCoordinates(String latitude,String longitude) {
        String[] coordinates = {latitude, longitude};
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getState(){
        return state;
    }

    public void setState(String contamination) {
        if (contamination.toUpperCase().startsWith("T")){
            contamination = "TRUE";
            this.state = contamination;

        }else {
            contamination = "FALSE";
        this.state = contamination;}
    }

    @Override
    public String toString() {
        return "{ \n" +
                "\"FarmID\": " + FarmID + ",\n" +
                "\"latitude\": " + latitude +",\n" +
                "\"longitude\": " + longitude +",\n" +
                "\"date\": " + date +",\n" +
                "\"state\": \"" + state + "\"\n" +
                '}';
    }
}
