let idArray = [];
let newObject = [];
rows = [];

// Get data
function getData() {
  req = new XMLHttpRequest();
  req.open("GET", '/mapdata', true);

  req.onloadstart = function () {
    $("#wait").css("display", "block");
    console.log("Loading...");
  };
  req.send();


  req.onload = function () {
    this.loading = true;
    dataset = JSON.parse(req.responseText);
    // Reduce data to unqinue farmid list for table 1
    let map = new Map();
    for (let item = 0; item < dataset.length; item++) {
      if (!map.has(dataset[item].FarmID)) {
        map.set(dataset[item].FarmID, true);    // set any value to Map
        idArray.push({
          FarmID: dataset[item].FarmID,
          latitude: dataset[item].latitude,
          longitude: dataset[item].longitude
        });
      }

    }

    // create new dataset with measurements per farm
    newObject = dataset.reduce(function (obj, value) {
      var key = `Farm ID: ${value.FarmID}`;
      if (obj[key] == null) obj[key] = [];
      obj[key].push(value);


      return obj;
    }, []);
  };
}

// Create table with farm id's
$.when(getData() ).done(
  $(document).ready(function() {
    $("#wait").css("display", "none");

    var table = $('#idTable').DataTable({
      "data": idArray,
      'processing': true,
      "columns": [
        {"data": "FarmID"},
        {"data": "latitude"},
        {"data": "longitude"}
      ],
      "columnDefs": [ {
        "targets": [1,2],
        "searchable": false
      } ]
    });

    $('#idTable')
      .on( 'processing.dt', function ( e, settings, processing ) {
          $('#wait').css( 'display', processing ? 'block' : 'none' );
        }
      )
      .dataTable();

    constructDataTable(rows); // Create table with measurements

    // Refresh measurement table every time a farm is clicked
    $('#idTable tbody').on('click', 'tr', function () {
      var data = table.row(this).data();
      let rows = newObject["Farm ID: " + data.FarmID];

      datatable = $('#dataTable').DataTable();
      datatable.clear();  // clear data before add new data
      datatable.rows.add(rows).draw();
    });
  }));




// Construct table with measuerements for each farm
function constructDataTable(dataset) {
  $('#dataTable').DataTable( {
    "data": dataset,
    "columns": [
      { "data": "FarmID"},
      { "data": "date",
        // Return the correct date
        render: function(d) {
          return moment(d).format("DD:MM:YYYY");
        }
      },
      { "data": "state"}
    ],
    "columnDefs": [
      { "width": "40", "targets": 1 },
      {"targets": 2,
        // Change color based on value
        render: function ( data, type, row ) {
          var color = 'black';
          if (data.toString().startsWith("T")) {
            color = 'green';
          }
          if (data.toString().startsWith("F")) {
            color = 'red';
          }
          return '<span style="color:' + color + '">' + data + '</span>';
        }}
    ]
  } );
}
