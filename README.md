# Web Application: Salmonella infections at dairy farms
![Version](https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0.html)

> "This web application shows the spread of salmonella through dairy farms in the province of Groningen."

## Table of contents
1. Purpose
2. Installation
3. Usage and Features
4. Suggestions for Future Updates
5. Bugs
6. Contact
7. License

## Purpose
This web application was commissioned by the dutch veterinary practice 'van Stad tot Wad', to give more insight into the spread of salmonella through dairy farms in the province of Groningen. The data is visualized in a map made with a with Geographical Information System (GIS). In this case the ArcGIS API for JavaScript (version 4.14). The Web application consists of a Java sever-side back-end, coupled to a dynamic web user-interface.

## Installation
> Note: This installation is for InteliJ IDE. The installation steps can differ for other IDE's.

#### 1. Clone the repository ####
```sh
git clone https://mlbloembergen@bitbucket.org/civannassau/salmonellosis.git
```

#### 2. Setup database: ####
A MYSQL database server is required for this project. If you do not have one available, you can create your own with the MYSQL installer in this link: `https://dev.mysql.com/downloads/windows/installer/8.0.html`.

- Step 1. Change SQLconnector class

  Change the database configuration in the SQLConnector class from the salmonellosis/java/service package, unless you are working from the bio-infomatics network at the Hanze Hogeschool.
  Note: the database name needs to be changed in the database url, just like the host.

  For example:

  Before:
```java
String dbURL = "jdbc:mysql://projectsdb.bin/salmonellosis?allowLoadLocalInfile=true";
```

  After:
```java
String dbURL = "jdbc:mysql://localhost:3306/MYSQL?allowLoadLocalInfile=true";
```
- Step 2. Add configuration to check your database connection.

    1. Select database in the side panel on the right.

    2. `(+) Add > Data Source > MySQL`

    3. Add your own database name, url password etc.

    4. Download missing driver files if neccessary

    5. Go to the Advanced tab

    6. Set AllowLoadLocalInfile to TRUE


#### 3. Setup  run configuration: ####
`Edit Configurations.. > Add (+) > Tomcat Server > Local > Give name for configuration > JRE: 11 > FIX warning >
Change URL`, for example if you want to open Gis file at first use: `http://localhost:8080/Gis`
Go to `deployment > Application context: delete text > Apply > Ok`

#### 4. Setup Tomcat: ####
This web application runs on a tomcat server, here follows the configuration:

- Step 1. Change the settings.
  File > Settings > Build, Execution, Deployment > Application Servers > Select at Tomcat Home >
  `salmonellosis/tomcat server/apache-tomcat-9.0.27`

- Step 2. Add tomcat to the run configuration.
  You can find an example in this link: `https://mkyong.com/intellij/intellij-idea-run-debug-web-application-on-tomcat/`. Tomcat is   included in the repository. When you select 'Configure..' in the run configuration, select the apache folder in the Tomcat server     directory (`tomcat server/apache-tomcat-9.0.27`).

- Step 3. Change permissions
In order to use tomcat, you will need to change the permissions. Follow these steps:
```sh
# Step 1. Go to the bin folder
cd tomcat\ server/apache-tomcat-9.0.27/bin
# Step 2. Change  the permissions
chmod 770 *.sh
```

#### 5. Edit Web.xml: ####
The absolute path of the data folder is given in the web.xml and needs to be changed to your own path.

- Step 1. Copy the absolute path of the data folder.
```sh
#Example of an absolute path
/Users/jildou/Documents/Salmonellsis/data
```

- Step 2. Change the path in parameter of the Fileupload servlet in the web.xml file.
```xml
<!--Before-->
  <servlet>
        <servlet-name>FileUploadServlet</servlet-name>
        <servlet-class>servlets.FileUploadServlet</servlet-class>
        <init-param>
            <param-name>data_dir</param-name>
            <!--Change the line below-->
            <param-value>/homes/mlbloembergen/thema10/practicum/salmonellosis/salmonellosis/data </param-value>
        </init-param>
    </servlet>

<!--After-->
   <servlet>
        <servlet-name>FileUploadServlet</servlet-name>
        <servlet-class>servlets.FileUploadServlet</servlet-class>
        <init-param>
            <param-name>data_dir</param-name>
            <!--This is the changed line-->
            <param-value>/Users/jildou/Documents/Salmonellsis/data</param-value>
        </init-param>
    </servlet>
```



## Usage and Features

The web application will start when you run the configuration (play button top right in InteliJ).
The browser will open the GIS/map page.

- The welcome page gives more information about the project.

- At the GIS page you can see the ArcGIS map with the time slider. The red dots are the contaminated
bulk tanks. A bright red dot means the farm is contaminated in the time frame selected on the time slider at the bottom, a vague red dot means the farm has been infected.
A popup will appear when a data point is selected. The popup displays information about the point such as: the ID, the date of the measurement, the number of infections and the coordinates. It also contains a graph displaying the historic overview of salmonella infections at the given farm. Another feature is the legend at the right, two layers can be chosen. One is a layer that shows the infected and non-infected farms and works with the time slider. The other displays clusters and can be useful if this project would use global dairy farm data.

- Data can be added via the Add new Data page, either a single farm instance
or a complete csv file can be submitted.
For the single instance, all the fields are required.
The fields are: Farm ID, coordinates ( there is a button which adds your local coordinates when selected), date ( easy to select with the calendar popup) and the contamination state.
When you want to add a file, it needs to be a csv file and you can shuffle the coordinates ( the shuffle is optional).

- The View data page gives more insight in the data. In the first table you can select a farm and even search for a farm ID in the search bar. When a farm is selected the second table will display the measurements taken at that farm (the date and the state of contamination).

## Suggestions for future updates
- A bar plot in the legend of the GIS map page, displaying the number of infected farms per year.
- Predicting the spread pattern of salmonella, maybe with machine learning

## Bugs
- Browser bug: The data table on the View Data page only works in Google Chrome, not in Firefox.

## Authors
This project is made by three students from the Hanze Hogeschool Groningen.

👤 **Liesje Bloembergen, Jildou Haaijer and Chenoa van Nassau.**

For any problems or questions:

📧 **m.l.bloembergen@st.hanze.nl, j.f.haaijer@st.hanze.nl and c.i.van.nassau@st.hanze.nl.**


## 📝 License

This project is [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) licensed.
